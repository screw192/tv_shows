import React from 'react';
import {Route, Switch} from "react-router-dom";
import {useSelector} from "react-redux";

import Preloader from "./components/UI/Preloader/Preloader";
import Header from "./components/Header/Header";
import SearchForm from "./components/SearchForm/SearchForm";
import TvShowCard from "./components/TVShowCard/TVShowCard";

const App = () => {
  const isLoading = useSelector(state => state.loading);

  let preloader;
  isLoading ? preloader = <Preloader/> : preloader = null;

  return (
      <>
        {preloader}
        <Header label={"TV Shows"}/>
        <SearchForm/>
        <Switch>
          <Route path="/" exact component={null}/>
          <Route path="/shows/:id" exact component={TvShowCard}/>
        </Switch>
      </>
  );
};

export default App;