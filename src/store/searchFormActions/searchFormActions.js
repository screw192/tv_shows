import axiosOrders from "../../axios-orders";

export const SEARCH_FIELD_FOCUS = "SEARCH_FIELD_FOCUS";

export const FETCH_TVSHOWS_REQUEST = "FETCH_TVSHOWS_REQUEST";
export const FETCH_TVSHOWS_SUCCESS = "FETCH_TVSHOWS_SUCCESS";
export const FETCH_TVSHOWS_FAILURE = "FETCH_TVSHOWS_FAILURE";

export const searchFieldFocus = inFocus => ({type: SEARCH_FIELD_FOCUS, inFocus});

export const fetchTvShowsRequest = () => ({type: FETCH_TVSHOWS_REQUEST});
export const fetchTvShowsSuccess = data => ({type: FETCH_TVSHOWS_SUCCESS, data});
export const fetchTvShowsFailure = () => ({type: FETCH_TVSHOWS_FAILURE});

export const fetchTvShows = searchValue => {
  return async dispatch => {
    dispatch(fetchTvShowsRequest());

    try {
      const response = await axiosOrders.get("search/shows?q=" + searchValue);
      dispatch(fetchTvShowsSuccess(response.data));
    } catch (e) {
      dispatch(fetchTvShowsFailure());
    }
  };
};