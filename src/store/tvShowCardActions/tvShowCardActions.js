import axiosOrders from "../../axios-orders";

export const FETCH_TARGET_SHOW_REQUEST = "FETCH_TARGET_SHOW_REQUEST";
export const FETCH_TARGET_SHOW_SUCCESS = "FETCH_TARGET_SHOW_SUCCESS";
export const FETCH_TARGET_SHOW_FAILURE = "FETCH_TARGET_SHOW_FAILURE";

export const fetchTargetShowRequest = () => ({type: FETCH_TARGET_SHOW_REQUEST});
export const fetchTargetShowSuccess = data => ({type: FETCH_TARGET_SHOW_SUCCESS, data});
export const fetchTargetShowFailure = () => ({type: FETCH_TARGET_SHOW_FAILURE});

export const fetchTargetShow = searchValue => {
  return async dispatch => {
    dispatch(fetchTargetShowRequest());

    try {
      const response = await axiosOrders.get("shows/" + searchValue);
      dispatch(fetchTargetShowSuccess(response.data));
    } catch (e) {
      dispatch(fetchTargetShowFailure());
    }
  };
};