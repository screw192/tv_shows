import {
  FETCH_TVSHOWS_FAILURE,
  FETCH_TVSHOWS_REQUEST,
  FETCH_TVSHOWS_SUCCESS,
  SEARCH_FIELD_FOCUS,
} from "./searchFormActions/searchFormActions";

import {
  FETCH_TARGET_SHOW_FAILURE,
  FETCH_TARGET_SHOW_REQUEST,
  FETCH_TARGET_SHOW_SUCCESS
} from "./tvShowCardActions/tvShowCardActions";

const initialState = {
  searchFieldFocus: false,
  showsList: [],
  targetShow: {},
  loading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_FIELD_FOCUS:
      return {...state, searchFieldFocus: action.inFocus};

    case FETCH_TVSHOWS_REQUEST:
      return state;
    case FETCH_TVSHOWS_SUCCESS:
      return {...state, showsList: action.data};
    case FETCH_TVSHOWS_FAILURE:
      return state;

    case FETCH_TARGET_SHOW_REQUEST:
      return {...state, loading: true};
    case FETCH_TARGET_SHOW_SUCCESS:
      return {...state, loading: false, targetShow: action.data};
    case FETCH_TARGET_SHOW_FAILURE:
      return {...state, loading: false};

    default:
      return state;
  }
};

export default reducer;