import React from 'react';
import {useDispatch} from "react-redux";
import {fetchTvShows, searchFieldFocus} from "../../store/searchFormActions/searchFormActions";
import Autocomplete from "./Autocomplete/Autocomplete";

import "./SearchForm.css";

const SearchForm = () => {
  const dispatch = useDispatch();

  const onInputChange = e => dispatch(fetchTvShows(e.target.value));

  const searchFieldOnFocus = () => dispatch(searchFieldFocus(true));
  const searchFieldOnBlur = () => {
    setTimeout(() => {
      dispatch(searchFieldFocus(false));
    }, 200);
  };

  return (
      <div className="container">
        <div className="SearchForm">
          <label
              htmlFor="tvShowSearch"
              className="SearchFormLabel"
          >Search for TV Show:</label>
          <div className="InputBlock">
            <input
                id="tvShowSearch"
                type="text"
                placeholder="Start typing something..."
                onChange={onInputChange}
                onFocus={searchFieldOnFocus}
                onBlur={searchFieldOnBlur}
            />
            <Autocomplete/>
          </div>
        </div>
      </div>
  );
};

export default SearchForm;