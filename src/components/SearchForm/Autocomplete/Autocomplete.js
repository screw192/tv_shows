import React from 'react';
import {useSelector} from "react-redux";
import {Link} from "react-router-dom";

import "./Autocomplete.css";

const Autocomplete = () => {
  const showsList = useSelector(state => state.showsList);
  const inputInFocus = useSelector(state => state.searchFieldFocus);

  const autocompleteList = showsList.map(item => {
    return (
        <li key={item.show.id} className="ShowListItem">
          <Link className={"ShowLink"} to={`/shows/${item.show.id}`}>{item.show.name}</Link>
        </li>
    );
  });

  let autoCompleteBlock = null;
  if (!!showsList.length && inputInFocus) {
    autoCompleteBlock = (
        <ul className="AutocompleteBlock">
          {autocompleteList}
        </ul>
    );
  }

  return autoCompleteBlock;
};

export default Autocomplete;