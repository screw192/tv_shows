import React from 'react';
import {Link} from "react-router-dom";

import "./Header.css";

const Header = props => {
  return (
      <>
        <header className="HeaderBlock">
          <div className="container">
            <div className="Header">
              <Link className="HomepageLink" to="/">{props.label}</Link>
            </div>
          </div>
        </header>
      </>
  );
};

export default Header;