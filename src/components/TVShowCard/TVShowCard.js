import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import parse from 'html-react-parser';

import {fetchTargetShow} from "../../store/tvShowCardActions/tvShowCardActions";
import "./TVShowCard.css";

const TvShowCard = props => {
  const dispatch = useDispatch();
  const {id, image, name, genres, status, weight, summary} = useSelector(state => state.targetShow);

  const showID = props.match.params.id;

  React.useEffect(() => {
    dispatch(fetchTargetShow(showID));
  }, [dispatch, showID]);

  let tvShowCover = (
      <div className="NoImagePlug">No cover found :(</div>
  );
  if (image) {
    tvShowCover = (
        <img
            className="TvShowImage"
            src={image.medium}
            alt={name}
            height="295px"
            width="210px"
        />
    );
  }

  const statusClasses = ["TvShowStatus"];
  switch (status) {
    case "Running":
      statusClasses.push("TvShowStatus_running");
      break;
    case "Ended":
      statusClasses.push("TvShowStatus_ended");
      break;
    default:
      break;
  }

  let tvShowCard = null;
  if (id) {
    tvShowCard = (
        <div className="container">
          <div className="TvShowCard">
            <div className="TvShowImage">
              {tvShowCover}
            </div>
            <div className="TvShowInfoBlock">
              <h2 className="TvShowInfo TvShowTitle">{name}<sup className={statusClasses.join(" ")}>{status}</sup></h2>
              <p className="TvShowInfo TvShowGenres"><b>Genre:</b> {genres.join(", ")}</p>
              <p className="TvShowInfo TvShowEpisodes"><b>{weight}</b> episodes</p>
              <div className="TvShowInfo TvShowSummary"><b>About: </b>{parse(summary)}</div>
            </div>
          </div>
        </div>
    );
  }

  return tvShowCard;
};

export default TvShowCard;