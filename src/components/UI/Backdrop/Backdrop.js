import React from 'react';

import "./Backdrop.css";

const Backdrop = props => {
  return (
      <div className="Backdrop" onClick={props.clickHandler}>
        {props.children}
      </div>
  );
};

export default Backdrop;